#include <NewPing.h>
#define ECHO_PIN1 4
#define TRIG_PIN1 6

#define ECHO_PIN2 8
#define TRIG_PIN2 10

#define ECHO_PIN3 12
#define TRIG_PIN3 5

#define ECHO_PIN4 7
#define TRIG_PIN4 9

#define MAX_DISTANCE 20

#define COMMS_1 11
#define COMMS_2 2
#define COMMS_3 3
#define COMMS_4 A1

NewPing Sonar1(TRIG_PIN1, ECHO_PIN1, MAX_DISTANCE);
NewPing Sonar2(TRIG_PIN2, ECHO_PIN2, MAX_DISTANCE);
NewPing Sonar3(TRIG_PIN3, ECHO_PIN3, MAX_DISTANCE);
NewPing Sonar4(TRIG_PIN4, ECHO_PIN4, MAX_DISTANCE);

bool pingFrontRightBool;
bool stopCommand;

void pingSensor2()
{
  unsigned long dist = Sonar2.ping_cm();
  stopCommand = (dist > 0) || stopCommand;
  digitalWrite(COMMS_2, dist > 0);

  Serial.print("Sensor 2:");
  Serial.println(dist);
}

void pingSensor3()
{
  unsigned long dist = Sonar3.ping_cm();
  stopCommand = (dist > 0) || stopCommand;
  digitalWrite(COMMS_3, dist > 0);

  Serial.print("Sensor 3:");
  Serial.println(dist);
}

void pingSensor4()
{
  unsigned long dist = Sonar4.ping_cm();
  stopCommand = (dist > 0) || stopCommand;
  digitalWrite(COMMS_4, dist > 0);

  Serial.print("Sensor 4:");
  Serial.println(dist);
}

void pingSensor1()
{
  unsigned long dist = Sonar1.ping_cm();
  stopCommand = dist > 0;
  digitalWrite(COMMS_1, dist > 0);
  digitalWrite(LED_BUILTIN, dist > 0);
  Serial.print("Sensor 1:");
  Serial.println(dist);
}

void setup()
{
  Serial.begin(9600);
  Serial.println("Init started! Strom sparen! ~Julius");

  pinMode(ECHO_PIN1, INPUT);
  pinMode(TRIG_PIN1, OUTPUT);

  pinMode(ECHO_PIN2, INPUT);
  pinMode(TRIG_PIN2, OUTPUT);

  pinMode(ECHO_PIN3, INPUT);
  pinMode(TRIG_PIN3, OUTPUT);

  pinMode(ECHO_PIN4, INPUT);
  pinMode(TRIG_PIN4, OUTPUT);

  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(COMMS_1, OUTPUT);
  digitalWrite(COMMS_1, LOW);
  pinMode(COMMS_2, OUTPUT);
  digitalWrite(COMMS_2, LOW);
  pinMode(COMMS_2, OUTPUT);
  digitalWrite(COMMS_2, LOW);
  pinMode(COMMS_2, OUTPUT);
  digitalWrite(COMMS_2, LOW);

  Serial.println("Init done! Strom wurde gespart :^");
}

void loop()
{
  stopCommand = false;
  pingSensor1();
  pingSensor2();
  pingSensor3();
  pingSensor4();
  digitalWrite(LED_BUILTIN, stopCommand);
}